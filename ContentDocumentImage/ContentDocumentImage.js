import { LightningElement, track } from "lwc";
import getContentVersionData from "@salesforce/apex/orca_News.getContentVersionData";
export default class ContentDocumentImage extends LightningElement {

  @track imgLink = "";
  @track docId = "docId";

  connectedCallback() {
    this.getNewsData();
  }
  getNewsData() {
    getContentVersionData({ ContentDocumentId: this.docId })
    .then(result => {
      let ContentVersionId = result;  // returns content Version Id
      this.imgLink =
        "/sfc/servlet.shepherd/version/download/" + ContentVersionId;
      console.log("URL::" + this.imgLink);
    })
    .catch(error => {
      console.log("Error while fetching contentversion id " + error);
    });
  }

}